
#  slot machine

A simple slot machine simulation. The frontend is written with no external library, css is written in plain CSS (no pre-processors used) and also JS code is written in ES6 but its now runs out of the box in Google Chrome and Firefox so there's no babel libraries included either. The server part is written in ExpressJS.

  
##  Setup:

To setup to work it locally:

```
npm install -g yarn
yarn install;
yarn start
open http://0.0.0.0:7000
```

This will start the express server at port `http://0.0.0.0:7000`
