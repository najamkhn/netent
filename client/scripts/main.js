const Utils = {
    /**
     * Generate a random integer (no floating points)
     * @param {int} max - upper limit for the integer
     * @param {int} min - lower limit for the integer
     */
    random(max, min = 0) {
        return Math.floor((Math.random() * max)) + min;
    },

    elem(id) {
        return document.getElementById(id);
    },

    /**
     * Generate and returns a DOM element
     * @param {string} tagName 
     * @param {string} classString 
     * @param {string} style 
     */
    generateElement(tagName, classString, style) {
        const el = document.createElement(tagName);
        
        el.className = classString;
        el.style     = style;
        // img.style.animation = `example2 ${rand}s ${rand2}`;
        return el;
    },

    /**
     * Generate and returns a IMG element
     * @param {string} classString 
     * @param {string|int} symbolIndex 
     * @param {string} style 
     */
    generateSymbols(classString, symbolIndex, style = null) {
        const el = this.generateElement('img', classString, style);
        el.src = `./images/Symbol_${symbolIndex}.png`;
        return el;
    },

    makeElemSeemBusy(elem) {
        elem.disabled = true;
        elem.style.opacity = 0.3;
    },

    makeElemSeemAvailable(elem) {
        elem.disabled = false;
        elem.style.opacity = 1;
    }

};

const Request = {
    endpoint: '/spin',

    async fetchData() {
        const response = await window.fetch(this.endpoint);
        return await response.json();
    }
};

const Main = {
    init() {
        document.querySelector('.retry').onclick = (event) => {
            Utils.makeElemSeemBusy(event.target);
            Request.fetchData().then(data => Main.processResponse(event, data));
        };
    },

    processResponse(event, resp) {
        const isBonus       = resp.bonus,
              randomNumbers = resp.data,
              winType       = resp.gameResult,
              target        = event.target;

        const header = document.querySelector('.heading span');
        header.innerHTML = 'Shuffling';

        let elems = Array.from(document.querySelectorAll('.item'));

        randomNumbers.map((item, index) => {
            const img = Utils.generateSymbols('img', item);
            document.querySelectorAll(`.item-container`)[index].classList.add(`item${index + 1}`);
            document.querySelectorAll(`.item-container`)[index].prepend(img);
        });


        setTimeout(() => {
            const el = document.querySelector('.heading span');
            
            // Add animation class
            el.setAttribute('style', 'animation: blink 2s linear');
            
            // Clear the style after 2 seconds
            setTimeout(() => el.setAttribute('style', ''), 2000);
            
            el.innerHTML = winType;
            Array.from(document.querySelectorAll(`.item-container`)).map((x, i) => x.classList.remove(`item${i+1}`));

            if (resp.bonus) {
                const bonusEl = document.querySelector('.bonus');
                bonusEl.removeAttribute('style')
                // Add animation class for the Bonus element
                bonusEl.style = 'display: block; opacity: 1; animation: bonus-animation 1s linear;'
    
                // remove the animation and other style classes
                setTimeout(() => bonusEl.setAttribute('style', ''), 1001);
                Utils.makeElemSeemAvailable(document.querySelector(`.retry`));
                document.querySelector('.retry').click();
            } else {
                Utils.makeElemSeemAvailable(document.querySelector(`.retry`));
            }
        }, 2200);
    }
}

// Let's play the game
window.onload = Main.init;