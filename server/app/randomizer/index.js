module.exports = () => {
    const length = 3, maxRandomNumber = 5, bonusProbability = 1;
    const getRandomInt    = max => Math.floor(Math.random() * Math.floor(max));
    const generateRandom  = () => getRandomInt(maxRandomNumber);
    const generateNumbers = fn => [fn(), fn(), fn()];
    const addBonus        = data => Object.assign({
        // Reducing the probability of bonus from being a fair 50% to 16%
        bonus: !getRandomInt(6)
    }, {
        data: data
    });

    const numbersData = generateNumbers(generateRandom);
    const distinct    = [...new Set(numbersData)];
    let   gameResult  = '';

    if (0 === distinct.length - numbersData.length) {
        gameResult = 'No Win';
    } else if (1 === Math.abs(distinct.length - numbersData.length)) {
        gameResult = 'Small Win';
    } else {
        gameResult = 'Big Win';
    }

    // console.log(Math.abs(distinct.length - numbersData.length), distinct.length, numbersData.length);
    return {...addBonus(numbersData), gameResult};
};
