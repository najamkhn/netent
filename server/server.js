const express = require('express');
const app = express();
const randomizer = require('./app/randomizer/');

const spin = (req, res) => res.send(randomizer());
app.get('/spin', spin);
app.use(express.static('./client'));

app.listen(7000, () => console.log('Example app listening on port 7000!'))